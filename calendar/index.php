<!doctype html>
	<html lang="en-GB">
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>Terence Eden's Calendar</title>
			<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
			<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
			<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
			<style>
			</style>
			<script>
				(function (C, A, L) {
				  let p = function (a, ar) {
					 a.q.push(ar);
				  };
				  let d = C.document;
				  C.Cal =
					 C.Cal ||
					 function () {
						let cal = C.Cal;
						let ar = arguments;
						if (!cal.loaded) {
						  cal.ns = {};
						  cal.q = cal.q || [];
						  d.head.appendChild(d.createElement("script")).src = A;
						  cal.loaded = true;
						}
						if (ar[0] === L) {
						  const api = function () {
							 p(api, arguments);
						  };
						  const namespace = ar[1];
						  api.q = api.q || [];
						  typeof namespace === "string"
							 ? (cal.ns[namespace] = api) && p(api, ar)
							 : p(cal, ar);
						  return;
						}
						p(cal, ar);
					 };
				})(window, "https://cal.com/embed.js", "init");
				Cal("init");
			</script>
		</head>
		<body>
			<main id="cal-dot-com">
				<noscript>
					<h1>Book a meeting with me</h1>
					<p>Browsing without JavaScript? I can respect that!
					<p>You can <a href="https://cal.com/edent">book a meeting with me using Cal.com</a>.
					<p>Or <a href="https://edent.tel/">drop me an email</a>.
				</noscript>
			</main>
			<script>
				Cal("inline", {
				elementOrSelector: "#cal-dot-com",
				calLink: "edent",
				config: {
					name: "Terence Eden", // Prefill Name
					theme: "light", // "dark" or "light" theme
				},
				});
			</script>
		</body>
</html>
